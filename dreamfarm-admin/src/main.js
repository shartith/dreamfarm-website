import Vue from 'vue';
import Vuex from 'vuex'
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import router from './router';
import store from './store';
import VueApexCharts from 'vue-apexcharts'
import axios from 'axios';
import VueCookies from "vue-cookies";


Vue.prototype.$Axios = axios;
Vue.use(VueApexCharts)
Vue.use(Vuex)
Vue.use(VueCookies)

Vue.component('apexchart', VueApexCharts)
Vue.config.productionTip = false;
Vue.use(Element, { size: 'small', zIndex: 3000 });

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
