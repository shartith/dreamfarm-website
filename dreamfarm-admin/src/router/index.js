import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/index.vue';
import Login from '../views/login.vue';
import CubeManage from '../views/cubeManage.vue';
import CubeManageDetail from '../views/cubeManageDetail.vue';
import ControllerManage from '../views/controllerManage.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
  },
  {
    path: '/index',
    name: 'index',
    component: Index,
  },
  {
    path: '/CubeManage',
    name: 'cubeManage',
    component: CubeManage,
  },
  {
    path: '/CubeManageDetail',
    name: 'cubeManageDetail',
    component: CubeManageDetail,
  },
  {
    path: '/ControllerManage',
    name: 'controllerManage',
    component: ControllerManage,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  // 라우터 변경 전 실행될 코드
  const token = Vue.$cookies.get("user_token")
  
  if((token == '' || token == null) && to.path !== '/'){
    next('/')
  } else if((token != '' && token != null) && to.path === '/'){
    next('/index')
  } else {
    next(); // 라우터 변경
  }
});

export default router;
