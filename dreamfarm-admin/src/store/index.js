import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from '@/router';
import VueCookies from "vue-cookies";

Vue.use(Vuex);
Vue.use(VueCookies);
Vue.use(router);
Vue.$cookies.config("1d");

export default new Vuex.Store({
  state: {
    user: {
      user_token : null,
      user_id: null,
      user_password : null
    },
    token:{
      user_token : null
    }
  },
  getters: {
  },
  mutations: {
    setUser(state, _user){
      state.user = _user;
    },
    setRoles(){

    }
  },
  actions: {
    login: ({ commit }, _user) => {
      return new Promise((resolve, reject) => {
        axios.post('http://52.79.223.31:8000/user/login', _user)
          .then(res => {
            const user = {
              user_token: res.data[0].result,
              user_id: _user.user_id
            };
            const token = res.data[0].result;
            if (token !== '') {
              Vue.$cookies.set("user_token", token, "1d");
              Vue.$cookies.set("user_id", _user.user_id, "1d");
              commit('setUser', user);
              resolve(res);

              alert('로그인 되었습니다.')
              router.push({name:'index'})
            } else {
              alert('입력한 정보가 틀렸습니다. 다시 시도해주세요.');
            }
          })
          .catch(error => {
            console.error(error);
            reject(new Error('로그인 처리 중 알수 없는 오류 발생'));
          })
      });
    },
    checkToken:({commit}, token) =>{
      axios.post('http://52.79.223.31:8000/user/readtoken', token)
      .then(res => {

      })
      .catch(error => {
        console.error(error);
        alert('토큰 체크 중 알 수 없는 오류 발생');
      })
    },
    resetToken(){
      this.state.token.user_token = ''
      Vue.$cookies.remove("user_token")
    },
    getUserInfo:({commit}, token) =>{
      axios.post('http://52.79.223.31:8000/user/readtoken', token)
      .then(res => {

      })
      .catch(error => {
        console.error(error);
        alert('토큰 체크 중 알 수 없는 오류 발생');
      })
    },
    logout(){
      if (Vue.$cookies.get('user_token') === '') {
        throw Error('LogOut: token is undefined!');
      }
      this.state.token.user_token = ''
      this.state.user.user_token = ''
      this.state.user.user_id = ''
      this.state.user.user_password = ''
      Vue.$cookies.remove("user_token")
      Vue.$cookies.remove("user_id")
    }
  },
  modules: {
  },
});
